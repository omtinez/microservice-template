"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const microservice_1 = require("microservice");
const minimist = require("minimist");
const url = require("url");
/**
 * Main entrypoint to start this service
 * @param opts Catch-all for parameters. Options include:
 *
 * `port` - TCP port where this service should run
 *
 * `db` - database location, defaults to in-memory database
 *
 * `orchestrator` - URL where microservice-orchestrator is running; must be full URL including
 * protocol. E.g. "http://127.0.0.1:3000"
 */
function main(opts = {}) {
    return __awaiter(this, void 0, void 0, function* () {
        // Parse command-line arguments
        const GLOBAL_STATS = {};
        const args = minimist(process.argv.slice(1));
        // Helper function used to read arguments
        function getopt(name) {
            return opts[name] || args[name];
        }
        // Initialize microservice
        const service = yield microservice_1.MicroService.create(getopt('db'));
        // Setup debugging
        process.on('unhandledRejection', err => service.log('E', err));
        // Routes setup
        service.route('/', (request, response) => {
            response.send('Hello World');
        });
        service.route('/echo/:message', (request, response) => {
            response.send({ message: request.params.message });
        });
        service.route('/echo', (request, response) => {
            response.send({ message: request.query.message });
        }, { method: 'GET', mandatoryQueryParameters: ['message'] });
        service.route('/status', (request, response) => {
            response.send({ status: 'OK', data: GLOBAL_STATS });
        });
        // Pick a random port
        service.start(parseInt(getopt('port')) || Math.floor(Math.random() * 8999) + 1000);
        // Maintenance loop
        const maintenance_loop = () => __awaiter(this, void 0, void 0, function* () {
            const rows = yield service.db.all(`SELECT timestamp, message FROM log WHERE level = 'E' ORDER BY timestamp DESC LIMIT 10`);
            const msgs = rows.map((row) => row.message);
            GLOBAL_STATS.last_errors = rows
                .filter((row, ix) => msgs.indexOf(row.message) === ix)
                .map((row) => `[${row.timestamp}] ${row.message}`);
            // Register with orchestrator if option was passed as argument
            if (getopt('orchestrator')) {
                const orchestrator_url = url.parse(getopt('orchestrator'));
                service.register(orchestrator_url, 'deal', { local: '/deal', remote: '/' });
                service.register(orchestrator_url, 'dealsfrom', { local: '/dealsfrom', remote: '/' });
            }
        });
        setInterval(maintenance_loop, 60000);
        maintenance_loop();
        return service;
    });
}
exports.main = main;
// If starting from the command line, begin execution
if (typeof require !== 'undefined' && require.main === module) {
    main();
}
