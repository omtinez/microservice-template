"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
const request = require("supertest");
const index_1 = require("../index");
let service;
describe('routes', () => {
    before(() => __awaiter(this, void 0, void 0, function* () {
        service = yield index_1.main();
    }));
    beforeEach(() => {
        service.stop();
        service.clearCache(true);
        service.start(Math.floor(Math.random() * 8999) + 1000);
    });
    afterEach(() => {
        service.stop();
    });
    describe('root', () => {
        it('OK', done => {
            request(service.server).get('/').expect(200, done);
        });
    });
    describe('echo', () => {
        it('repeat query', done => {
            const rnd = '' + Math.floor(Math.random() * 9999);
            request(service.server).get('/echo?message=' + rnd).then(res => {
                assert.deepEqual(JSON.parse(res.text), { message: rnd });
                done();
            });
        });
        it('repeat param', done => {
            const rnd = '' + Math.floor(Math.random() * 9999);
            request(service.server).get('/echo/' + rnd).then(res => {
                assert.deepEqual(JSON.parse(res.text), { message: rnd });
                done();
            });
        });
    });
});
