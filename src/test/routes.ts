import { MicroService } from 'microservice'
import * as assert from 'assert';
import * as mocha from 'mocha';
import * as request from 'supertest';
import { main } from '../index';
let service: MicroService;

describe('routes', () => {

    before(async () => {
        service = await main();
    });

    beforeEach(() => {
        service.stop();
        service.clearCache(true);
        service.start(Math.floor(Math.random() * 8999) + 1000);
    });
    afterEach(() => {
        service.stop();
    });

    describe('root', () => {

        it('OK', done => {
            request(service.server).get('/').expect(200, done);
        });
    });

    describe('echo', () => {

        it('repeat query', done => {
            const rnd = '' + Math.floor(Math.random() * 9999);
            request(service.server).get('/echo?message=' + rnd).then(res => {
                assert.deepEqual(JSON.parse(res.text), {message: rnd});
                done();
            });
        });

        it('repeat param', done => {
            const rnd = '' + Math.floor(Math.random() * 9999);
            request(service.server).get('/echo/' + rnd).then(res => {
                assert.deepEqual(JSON.parse(res.text), {message: rnd});
                done();
            });
        });
    });
});