import { MicroService } from 'microservice';
import * as minimist from 'minimist';
import * as url from 'url';

/**
 * Main entrypoint to start this service
 * @param opts Catch-all for parameters. Options include:
 * 
 * `port` - TCP port where this service should run
 * 
 * `db` - database location, defaults to in-memory database
 * 
 * `orchestrator` - URL where microservice-orchestrator is running; must be full URL including
 * protocol. E.g. "http://127.0.0.1:3000"
 */
export async function main(opts: {[key: string]: any} = {}): Promise<MicroService> {

    // Parse command-line arguments
    const GLOBAL_STATS: {[key: string]: any} = {};
    const args = minimist(process.argv.slice(1));

    // Helper function used to read arguments
    function getopt(name: string): string {
        return opts[name] || args[name];
    }

    // Initialize microservice
    const service = await MicroService.create(getopt('db'));

    // Setup debugging
    process.on('unhandledRejection', err => service.log('E', err));

    // Routes setup
    service.route('/', (request, response) => {
        response.send('Hello World');
    });
    service.route('/echo/:message', (request, response) => {
        response.send({message: request.params.message});
    });
    service.route('/echo', (request, response) => {
        response.send({message: request.query.message});
    }, {method: 'GET', mandatoryQueryParameters: ['message']});
    service.route('/status', (request, response) => {
        response.send({status: 'OK', data: GLOBAL_STATS});
    });

    // Pick a random port
    service.start(parseInt(getopt('port')) || Math.floor(Math.random() * 8999) + 1000);

    // Maintenance loop
    const maintenance_loop = async () => {
        const rows = await service.db.all(
            `SELECT timestamp, message FROM log WHERE level = 'E' ORDER BY timestamp DESC LIMIT 10`);
        const msgs = rows.map((row: any) => row.message);
        GLOBAL_STATS.last_errors = rows
            .filter((row: any, ix) => msgs.indexOf(row.message) === ix)
            .map((row: any) => `[${row.timestamp}] ${row.message}`);

        // Register with orchestrator if option was passed as argument
        if (getopt('orchestrator')) {
            const orchestrator_url = url.parse(getopt('orchestrator'));
            service.register(orchestrator_url, 'deal', {local: '/deal', remote: '/'});
            service.register(orchestrator_url, 'dealsfrom', {local: '/dealsfrom', remote: '/'});
        }
    };
    setInterval(maintenance_loop, 60000);
    maintenance_loop();

    return service;
}

// If starting from the command line, begin execution
if (typeof require !== 'undefined' && require.main === module) {
    main();
}
